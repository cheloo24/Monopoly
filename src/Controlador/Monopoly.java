/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import GUI.Tablero;
import GUI.VentanaInicial;
import Modelo.Propiedades;
import java.io.IOException;
import java.sql.SQLException;


public class Monopoly {
    static Propiedades propiedad;
    static VentanaInicial vi;
    
    public static void main(String[] args) throws IOException, SQLException {
        propiedad = new Propiedades();
        vi = new VentanaInicial();
        vi.setVisible(true);
        propiedad.conectar();
        propiedad.CreaPropiedades();
    }
}
