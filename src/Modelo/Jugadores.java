/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Chelo
 */
public class Jugadores {
    String Nombre;
    int saldo;
    int propiedades;
    boolean turno;
    int posicion;

    public Jugadores() {
        turno = false;
        saldo = 2500;
        propiedades = 0;
        posicion = 1;
        
    }

    public Jugadores(String Nombre) {
        this.Nombre = Nombre;
        turno = false;
        saldo = 2500;
        propiedades = 0;
        posicion = 1;
        
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public int getSaldo() {
        return saldo;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }

    public int getPropiedades() {
        return propiedades;
    }

    public void setPropiedades(int propiedades) {
        this.propiedades = propiedades;
    }

    public int getPosicion() {
        return posicion;
    }

    public void setPosicion(int posicion) {
        this.posicion = posicion;
    }

 
   

    public boolean isTurno() {
        return turno;
    }

    public void setTurno(boolean turno) {
        this.turno = turno;
    }
    
    
}
