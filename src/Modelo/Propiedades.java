
package Modelo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author Chelo
 */
public class Propiedades extends ConexionTablero{
    

    public Propiedades() {
    
    }

    public void CreaPropiedades() throws IOException, SQLException{       
       try{
           String consultas = "select * from Casillas;";
           ResultSet rs = consulta.executeQuery(consultas);         
           File archivo = new File ("propiedades.txt");
           FileReader fr = new FileReader(archivo);
           BufferedReader br = new BufferedReader(fr);
           int contador = 1;
           while(br.ready()){
               if (contador != 1 && contador != 3 && contador != 8 && contador != 11 && contador != 18 && contador != 21 && contador != 23 && contador != 31 && contador != 34 && contador != 37){
                   String linea = "'"+br.readLine()+"'";
                   String sql = "UPDATE casillas set Nombre = "+linea+" where Numero="+contador+";";
                   consulta.executeUpdate(sql);
                   linea = "'"+br.readLine()+"'";
                   sql = "UPDATE casillas set Precio = "+linea+" where Numero = "+contador+";";
                   consulta.executeUpdate(sql);
                   linea = "'"+br.readLine()+"'";
                   sql = "UPDATE casillas set Propietario = "+linea+" where Numero="+contador+";";
                   consulta.executeUpdate(sql);
                   linea = "'"+br.readLine()+"'";
                   sql = "UPDATE casillas set Renta = "+linea+" where Numero="+contador+";";
                   consulta.executeUpdate(sql);
               }
               contador++;
           }
       }
       catch(FileNotFoundException fnfe){
           System.out.println(fnfe.getMessage());
       }
       catch (SQLException ex){
            JOptionPane.showMessageDialog(null, "Problemas al consultar la base de datos.");
       }
   }
    
}
