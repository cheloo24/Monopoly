package Modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author Kevin Canales
 */
public class ConexionJugador {
    Connection conexion;
    Statement consulta;
    private String ruta;
    
public ConexionJugador(){
        ruta = "Jugadores.bd";
 }

public void conectar(){
	try {
            Class.forName("org.sqlite.JDBC");
	}
	catch (ClassNotFoundException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
	}	 
            try {
                conexion = DriverManager.getConnection("jdbc:sqlite:"+ruta);
                consulta = conexion.createStatement();
            } 
            catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e.getMessage());
            }
}
    

public void ingresarJugador(String nombre, int dinero){
        conectar();
        try {
            String consultas = "select * from Jugadores;";
            ResultSet rs = consulta.executeQuery(consultas);
            JOptionPane.showMessageDialog(null, "aun estoy bien");
            String sql = "INSERT INTO Jugadores (Nombre, Dinero, Propiedades, VecesGanadas) "+"VALUES ('"+nombre+"','"+dinero+"', 0, 0);";
            consulta.executeUpdate(sql);
        }
        catch(SQLException ex){
            JOptionPane.showMessageDialog(null, "Problemas al consultar la base de datos.");
        }
}
    
    public String actualizarCompra(String nombre, int dinero, int propiedades){
       conectar();
        try {
            String consultas = "select * from Jugadores;";
            ResultSet rs = consulta.executeQuery(consultas);
            while(rs.next()){
                if (rs.getString("Nombre").compareTo(nombre) == 0){
                    String dineroString = rs.getString("Dinero");
                    String propiedadesString = rs.getString("Propiedades");
                    int Dinero = Integer.parseInt(dineroString);
                    int dineroFinal = Dinero - dinero;
                    int Propiedades = Integer.parseInt(propiedadesString);
                    int propiedadesFinal = Propiedades + propiedades;
                    dineroString = Integer.toString(dineroFinal);
                    String sql = "UPDATE Jugadores set Dinero = "+dineroString+" where Nombre='"+nombre+"';";
                    consulta.executeUpdate(sql);
                    sql = "UPDATE Jugadores set Propiedades = "+propiedadesString+"where Nombre='"+nombre+"';";
                    consulta.executeUpdate(sql);
                    return dineroString;
                }
            }
        }
        catch (SQLException ex){
            JOptionPane.showMessageDialog(null, "Problemas al consultar la base de datos.");
        }
    return null;  
    }
    
    public boolean puedePagar(String nombre, int costo){
        conectar();
        try {
            String consultas = "select * from Jugadores;";
            ResultSet rs = consulta.executeQuery(consultas);
            while(rs.next()){
                if (rs.getString("Nombre").compareTo(nombre) == 0){
                    String dineroString = rs.getString("Dinero");
                    int Dinero = Integer.parseInt(dineroString);
                    if (Dinero >= costo){
                        return true;
                    }
                }
            }
        }
        catch (SQLException ex){
            JOptionPane.showMessageDialog(null, "Problemas al consultar la base de datos.");
        }
    return false;
    }
    
    public String actualizaSaldo (String nombre, int costo){
        conectar();
        try {
            String consultas = "select * from Jugadores;";
            ResultSet rs = consulta.executeQuery(consultas);
            while(rs.next()){
                if (rs.getString("Nombre").compareTo(nombre) == 0){
                    String dineroString = rs.getString("Dinero");
                    int Dinero = Integer.parseInt(dineroString);
                    int dineroFinal = Dinero - costo;
                    dineroString = Integer.toString(dineroFinal);
                    String sql = "UPDATE Jugadores set Dinero = "+dineroString+" where Nombre='"+nombre+"';";
                    consulta.executeUpdate(sql);
                    return dineroString;
                }
            }
        }
        catch (SQLException ex){
            JOptionPane.showMessageDialog(null, "Problemas al consultar la base de datos.");
        }
    return null;
    }
}