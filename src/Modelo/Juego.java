/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Modelo;
import GUI.*;
import javax.swing.JOptionPane;

/**
 *
 * @author Chelo
 */
public class Juego extends VentanaInicial{
    public static int valorDado;
    public static int cantidadJugadores;
     
    
    public void ManejaTurno(){
         boolean hayGanador = Ganador();
         Tablero tablero = new Tablero();
         tablero.setVisible(true);
         while(hayGanador== false){     
             if(cantidadJugadores == 2){
                VentanaInicial.jugador1.setTurno(true);             
                tablero.BtnTirarDados.setEnabled(true);
                VentanaInicial.jugador1.setPosicion(VentanaInicial.jugador1.getPosicion()+ valorDado);
                if(VentanaInicial.jugador1.getPosicion()>40){
                    int temp = VentanaInicial.jugador1.getPosicion()%40;//resto y nueva posicion                
                    VentanaInicial.jugador1.setPosicion(temp);
                    VentanaInicial.jugador1.setSaldo(VentanaInicial.jugador1.getSaldo()+1000);
                }
                VentanaInicial.jugador1.setTurno(false);
                
                VentanaInicial.jugador2.setTurno(true);             
                tablero.BtnTirarDados.setEnabled(true);
                VentanaInicial.jugador2.setPosicion(VentanaInicial.jugador2.getPosicion()+ valorDado);
                if(VentanaInicial.jugador2.getPosicion()>40){
                    int temp = VentanaInicial.jugador2.getPosicion()%40;//resto y nueva posicion                
                    VentanaInicial.jugador2.setPosicion(temp);
                    VentanaInicial.jugador2.setSaldo(VentanaInicial.jugador2.getSaldo()+1000);
                }
                VentanaInicial.jugador2.setTurno(false);
                hayGanador = Ganador();
             }
             else if(cantidadJugadores == 3){
                 VentanaInicial.jugador1.setTurno(true);             
                tablero.BtnTirarDados.setEnabled(true);
                VentanaInicial.jugador1.setPosicion(VentanaInicial.jugador1.getPosicion()+ valorDado);
                if(VentanaInicial.jugador1.getPosicion()>40){
                    int temp = VentanaInicial.jugador1.getPosicion()%40;//resto y nueva posicion                
                    VentanaInicial.jugador1.setPosicion(temp);
                    VentanaInicial.jugador1.setSaldo(VentanaInicial.jugador1.getSaldo()+1000);
                }
               // VentanaInicial.jugador1.setTurno(false);
             
                VentanaInicial.jugador2.setTurno(true);             
                tablero.BtnTirarDados.setEnabled(true);
                VentanaInicial.jugador2.setPosicion(VentanaInicial.jugador2.getPosicion()+ valorDado);
                if(VentanaInicial.jugador2.getPosicion()>40){
                    int temp = VentanaInicial.jugador2.getPosicion()%40;//resto y nueva posicion                
                    VentanaInicial.jugador2.setPosicion(temp);
                    VentanaInicial.jugador2.setSaldo(VentanaInicial.jugador2.getSaldo()+1000);
                }
                VentanaInicial.jugador2.setTurno(false);
                
                VentanaInicial.jugador3.setTurno(true);             
                tablero.BtnTirarDados.setEnabled(true);
                VentanaInicial.jugador3.setPosicion(VentanaInicial.jugador3.getPosicion()+ valorDado);
                if(VentanaInicial.jugador3.getPosicion()>40){
                    int temp = VentanaInicial.jugador3.getPosicion()%40;//resto y nueva posicion                
                    VentanaInicial.jugador3.setPosicion(temp);
                    VentanaInicial.jugador3.setSaldo(VentanaInicial.jugador3.getSaldo()+1000);
                }
                VentanaInicial.jugador3.setTurno(false);
                hayGanador = Ganador();
             }
             else if(cantidadJugadores == 4){
                 
                 VentanaInicial.jugador1.setTurno(true);             
                tablero.BtnTirarDados.setEnabled(true);
                VentanaInicial.jugador1.setPosicion(VentanaInicial.jugador1.getPosicion()+ valorDado);
                if(VentanaInicial.jugador1.getPosicion()>40){
                    int temp = VentanaInicial.jugador1.getPosicion()%40;//resto y nueva posicion                
                    VentanaInicial.jugador1.setPosicion(temp);
                    VentanaInicial.jugador1.setSaldo(VentanaInicial.jugador1.getSaldo()+1000);
                }
                VentanaInicial.jugador1.setTurno(false);
                
                VentanaInicial.jugador2.setTurno(true);             
                tablero.BtnTirarDados.setEnabled(true);
                VentanaInicial.jugador2.setPosicion(VentanaInicial.jugador2.getPosicion()+ valorDado);
                if(VentanaInicial.jugador2.getPosicion()>40){
                    int temp = VentanaInicial.jugador2.getPosicion()%40;//resto y nueva posicion                
                    VentanaInicial.jugador2.setPosicion(temp);
                    VentanaInicial.jugador2.setSaldo(VentanaInicial.jugador2.getSaldo()+1000);
                }
                VentanaInicial.jugador2.setTurno(false);
                
                VentanaInicial.jugador3.setTurno(true);             
                tablero.BtnTirarDados.setEnabled(true);
                VentanaInicial.jugador3.setPosicion(VentanaInicial.jugador3.getPosicion()+ valorDado);
                if(VentanaInicial.jugador3.getPosicion()>40){
                    int temp = VentanaInicial.jugador3.getPosicion()%40;//resto y nueva posicion                
                    VentanaInicial.jugador3.setPosicion(temp);
                    VentanaInicial.jugador3.setSaldo(VentanaInicial.jugador3.getSaldo()+1000);
                }
                VentanaInicial.jugador3.setTurno(false);
                
                 VentanaInicial.jugador4.setTurno(true);             
                tablero.BtnTirarDados.setEnabled(true);
                VentanaInicial.jugador4.setPosicion(VentanaInicial.jugador4.getPosicion()+ valorDado);
                if(VentanaInicial.jugador4.getPosicion()>40){
                    int temp = VentanaInicial.jugador4.getPosicion()%40;//resto y nueva posicion                
                    VentanaInicial.jugador4.setPosicion(temp);
                    VentanaInicial.jugador4.setSaldo(VentanaInicial.jugador4.getSaldo()+1000);
                }
                VentanaInicial.jugador4.setTurno(false);
                hayGanador = Ganador();
                 
             }
             else if(cantidadJugadores == 5){
                 
                 VentanaInicial.jugador1.setTurno(true);             
                tablero.BtnTirarDados.setEnabled(true);
                VentanaInicial.jugador1.setPosicion(VentanaInicial.jugador1.getPosicion()+ valorDado);
                if(VentanaInicial.jugador1.getPosicion()>40){
                    int temp = VentanaInicial.jugador1.getPosicion()%40;//resto y nueva posicion                
                    VentanaInicial.jugador1.setPosicion(temp);
                    VentanaInicial.jugador1.setSaldo(VentanaInicial.jugador1.getSaldo()+1000);
                }
                VentanaInicial.jugador1.setTurno(false);
                
                VentanaInicial.jugador2.setTurno(true);             
                tablero.BtnTirarDados.setEnabled(true);
                VentanaInicial.jugador2.setPosicion(VentanaInicial.jugador2.getPosicion()+ valorDado);
                if(VentanaInicial.jugador2.getPosicion()>40){
                    int temp = VentanaInicial.jugador2.getPosicion()%40;//resto y nueva posicion                
                    VentanaInicial.jugador2.setPosicion(temp);
                    VentanaInicial.jugador2.setSaldo(VentanaInicial.jugador2.getSaldo()+1000);
                }
                VentanaInicial.jugador2.setTurno(false);
                
                VentanaInicial.jugador3.setTurno(true);             
                tablero.BtnTirarDados.setEnabled(true);
                VentanaInicial.jugador3.setPosicion(VentanaInicial.jugador3.getPosicion()+ valorDado);
                if(VentanaInicial.jugador3.getPosicion()>40){
                    int temp = VentanaInicial.jugador3.getPosicion()%40;//resto y nueva posicion                
                    VentanaInicial.jugador3.setPosicion(temp);
                    VentanaInicial.jugador3.setSaldo(VentanaInicial.jugador3.getSaldo()+1000);
                }
                VentanaInicial.jugador3.setTurno(false);
                
                 VentanaInicial.jugador4.setTurno(true);             
                tablero.BtnTirarDados.setEnabled(true);
                VentanaInicial.jugador4.setPosicion(VentanaInicial.jugador4.getPosicion()+ valorDado);
                if(VentanaInicial.jugador4.getPosicion()>40){
                    int temp = VentanaInicial.jugador4.getPosicion()%40;//resto y nueva posicion                
                    VentanaInicial.jugador4.setPosicion(temp);
                    VentanaInicial.jugador4.setSaldo(VentanaInicial.jugador4.getSaldo()+1000);
                }
                VentanaInicial.jugador4.setTurno(false);
                
                VentanaInicial.jugador5.setTurno(true);             
                tablero.BtnTirarDados.setEnabled(true);
                VentanaInicial.jugador5.setPosicion(VentanaInicial.jugador5.getPosicion()+ valorDado);
                if(VentanaInicial.jugador5.getPosicion()>40){
                    int temp = VentanaInicial.jugador5.getPosicion()%40;//resto y nueva posicion                
                    VentanaInicial.jugador5.setPosicion(temp);
                    VentanaInicial.jugador5.setSaldo(VentanaInicial.jugador5.getSaldo()+1000);
                }
                VentanaInicial.jugador5.setTurno(false);
                hayGanador = Ganador();
             }
             else if(cantidadJugadores == 6){
                 
                 VentanaInicial.jugador1.setTurno(true);             
                tablero.BtnTirarDados.setEnabled(true);
                VentanaInicial.jugador1.setPosicion(VentanaInicial.jugador1.getPosicion()+ valorDado);
                if(VentanaInicial.jugador1.getPosicion()>40){
                    int temp = VentanaInicial.jugador1.getPosicion()%40;//resto y nueva posicion                
                    VentanaInicial.jugador1.setPosicion(temp);
                    VentanaInicial.jugador1.setSaldo(VentanaInicial.jugador1.getSaldo()+1000);
                }
                //VentanaInicial.jugador1.setTurno(false);
                
                VentanaInicial.jugador2.setTurno(true);             
                tablero.BtnTirarDados.setEnabled(true);
                VentanaInicial.jugador2.setPosicion(VentanaInicial.jugador2.getPosicion()+ valorDado);
                if(VentanaInicial.jugador2.getPosicion()>40){
                    int temp = VentanaInicial.jugador2.getPosicion()%40;//resto y nueva posicion                
                    VentanaInicial.jugador2.setPosicion(temp);
                    VentanaInicial.jugador2.setSaldo(VentanaInicial.jugador2.getSaldo()+1000);
                }
                //VentanaInicial.jugador2.setTurno(false);
                
                VentanaInicial.jugador3.setTurno(true);             
                tablero.BtnTirarDados.setEnabled(true);
                VentanaInicial.jugador3.setPosicion(VentanaInicial.jugador3.getPosicion()+ valorDado);
                if(VentanaInicial.jugador3.getPosicion()>40){
                    int temp = VentanaInicial.jugador3.getPosicion()%40;//resto y nueva posicion                
                    VentanaInicial.jugador3.setPosicion(temp);
                    VentanaInicial.jugador3.setSaldo(VentanaInicial.jugador3.getSaldo()+1000);
                }
                //VentanaInicial.jugador3.setTurno(false);
                
                 VentanaInicial.jugador4.setTurno(true);             
                tablero.BtnTirarDados.setEnabled(true);
                VentanaInicial.jugador4.setPosicion(VentanaInicial.jugador4.getPosicion()+ valorDado);
                if(VentanaInicial.jugador4.getPosicion()>40){
                    int temp = VentanaInicial.jugador4.getPosicion()%40;//resto y nueva posicion                
                    VentanaInicial.jugador4.setPosicion(temp);
                    VentanaInicial.jugador4.setSaldo(VentanaInicial.jugador4.getSaldo()+1000);
                }
                //VentanaInicial.jugador4.setTurno(false);
                
                VentanaInicial.jugador5.setTurno(true);             
                tablero.BtnTirarDados.setEnabled(true);
                VentanaInicial.jugador5.setPosicion(VentanaInicial.jugador5.getPosicion()+ valorDado);
                if(VentanaInicial.jugador5.getPosicion()>40){
                    int temp = VentanaInicial.jugador5.getPosicion()%40;//resto y nueva posicion                
                    VentanaInicial.jugador5.setPosicion(temp);
                    VentanaInicial.jugador5.setSaldo(VentanaInicial.jugador5.getSaldo()+1000);
                }
                //VentanaInicial.jugador5.setTurno(false);
                
                VentanaInicial.jugador6.setTurno(true);             
                tablero.BtnTirarDados.setEnabled(true);
                VentanaInicial.jugador6.setPosicion(VentanaInicial.jugador6.getPosicion()+ valorDado);
                if(VentanaInicial.jugador6.getPosicion()>40){
                    int temp = VentanaInicial.jugador6.getPosicion()%40;//resto y nueva posicion                
                    VentanaInicial.jugador6.setPosicion(temp);
                    VentanaInicial.jugador6.setSaldo(VentanaInicial.jugador6.getSaldo()+1000);
                }
                //VentanaInicial.jugador6.setTurno(false);
                hayGanador = Ganador();
             }
             
             
         }
         
         
     }
     public static boolean Ganador(){ // Verifica Ganador Para un Juego Completo
         int saldoJ1 = VentanaInicial.jugador1.getSaldo();
         int saldoJ2 = VentanaInicial.jugador2.getSaldo();
         int saldoJ3 = VentanaInicial.jugador3.getSaldo();
         int saldoJ4 = VentanaInicial.jugador4.getSaldo();
         int saldoJ5 = VentanaInicial.jugador5.getSaldo();
         int saldoJ6 = VentanaInicial.jugador6.getSaldo();
         if(cantidadJugadores == 2){
             if(saldoJ1 == 0 || saldoJ2 == 0)
                 return true;
             else return false;
             
         }else if(cantidadJugadores == 3){
             if(saldoJ1 ==0 || saldoJ2 ==0 || saldoJ3 == 0)
                 return true;
             else return false;             
         }
         else if(cantidadJugadores == 4){
             if(saldoJ1 == 0 || saldoJ2 == 0 || saldoJ3 == 0 || saldoJ4 == 0)
                 return true;
             else return false;
         }
         else if(cantidadJugadores == 5){
             if(saldoJ1 == 0 || saldoJ2 == 0 || saldoJ3 == 0 || saldoJ4 == 0 || saldoJ5 == 0)
                 return true;
             else return false;             
         }
         else if(cantidadJugadores == 6){
             if(saldoJ1 == 0 || saldoJ2 == 0 || saldoJ3 == 0 || saldoJ4 == 0 || saldoJ5 == 0 || saldoJ6 == 0)
                 return true;
             else return false;
         }
         return false;
         
     }
     public static void GanadorCierre(){
         int saldoJ1 = VentanaInicial.jugador1.getSaldo();
         int saldoJ2 = VentanaInicial.jugador2.getSaldo();
         int saldoJ3 = VentanaInicial.jugador3.getSaldo();
         int saldoJ4 = VentanaInicial.jugador4.getSaldo();
         int saldoJ5 = VentanaInicial.jugador5.getSaldo();
         int saldoJ6 = VentanaInicial.jugador6.getSaldo();
         
         if(cantidadJugadores == 2){
             if(saldoJ1 > saldoJ2)
                 JOptionPane.showConfirmDialog(null, VentanaInicial.jugador1.getNombre()+" es el GANADOR!!!!");
             else JOptionPane.showConfirmDialog(null, VentanaInicial.jugador2.getNombre()+" es el GANADOR!!!!");
             
         }
         else if(cantidadJugadores == 3){
             if(saldoJ1 > saldoJ2 && saldoJ1 >saldoJ3)
                 JOptionPane.showConfirmDialog(null, VentanaInicial.jugador1.getNombre()+" es el GANADOR!!!!");
             else if(saldoJ2 > saldoJ1 && saldoJ2 > saldoJ3)
                 JOptionPane.showConfirmDialog(null, VentanaInicial.jugador2.getNombre()+" es el GANADOR!!!!");
             else if(saldoJ3 > saldoJ2 && saldoJ3 > saldoJ1){
                 JOptionPane.showConfirmDialog(null, VentanaInicial.jugador3.getNombre()+" es el GANADOR!!!!");
             }
                           
         }
         else if(cantidadJugadores == 4){
             if(saldoJ1 > saldoJ2 && saldoJ1 > saldoJ3 && saldoJ1 > saldoJ4)
                 JOptionPane.showConfirmDialog(null, VentanaInicial.jugador1.getNombre()+" es el GANADOR!!!!");
             else if(saldoJ2 > saldoJ3 && saldoJ2 > saldoJ1 && saldoJ2 > saldoJ4)
                 JOptionPane.showConfirmDialog(null, VentanaInicial.jugador2.getNombre()+" es el GANADOR!!!!");
             else if(saldoJ3 > saldoJ1 && saldoJ3 > saldoJ2 && saldoJ3 > saldoJ4)
                 JOptionPane.showConfirmDialog(null, VentanaInicial.jugador3.getNombre()+" es el GANADOR!!!!");
             else if(saldoJ4 > saldoJ1 && saldoJ4 > saldoJ2 && saldoJ4 > saldoJ3)
                 JOptionPane.showConfirmDialog(null, VentanaInicial.jugador4.getNombre()+" es el GANADOR!!!!");                 
         }
         else if(cantidadJugadores == 5){
             if(saldoJ1 > saldoJ2 && saldoJ1> saldoJ3 && saldoJ1> saldoJ4 && saldoJ1 > saldoJ5)
                 JOptionPane.showConfirmDialog(null, VentanaInicial.jugador1.getNombre()+" es el GANADOR!!!!");
             else if(saldoJ2 > saldoJ1 && saldoJ2> saldoJ3 && saldoJ2> saldoJ4 && saldoJ2 > saldoJ5){
                 JOptionPane.showConfirmDialog(null, VentanaInicial.jugador2.getNombre()+" es el GANADOR!!!!");
             }
             else if(saldoJ3 > saldoJ1 && saldoJ3 > saldoJ2 && saldoJ3 > saldoJ4 && saldoJ3 > saldoJ5)
                 JOptionPane.showConfirmDialog(null, VentanaInicial.jugador3.getNombre()+" es el GANADOR!!!!");
             else if(saldoJ4 > saldoJ1 && saldoJ4 > saldoJ2 && saldoJ4 > saldoJ3 && saldoJ4 > saldoJ5){
                 JOptionPane.showConfirmDialog(null, VentanaInicial.jugador4.getNombre()+" es el GANADOR!!!!");
             }
             else if(saldoJ5 > saldoJ1 && saldoJ5 > saldoJ2 && saldoJ5 > saldoJ3 && saldoJ5 > saldoJ4){
                 JOptionPane.showConfirmDialog(null, VentanaInicial.jugador5.getNombre()+" es el GANADOR!!!!"); 
             }
         }
         else if(cantidadJugadores == 6){
             if(saldoJ1 > saldoJ2 && saldoJ1> saldoJ3 && saldoJ1> saldoJ4 && saldoJ1 > saldoJ5 && saldoJ1 > saldoJ6)
                 JOptionPane.showConfirmDialog(null, VentanaInicial.jugador1.getNombre()+" es el GANADOR!!!!");
             else if(saldoJ2 > saldoJ1 && saldoJ2> saldoJ3 && saldoJ2> saldoJ4 && saldoJ2 > saldoJ5 && saldoJ2 > saldoJ6){
                 JOptionPane.showConfirmDialog(null, VentanaInicial.jugador2.getNombre()+" es el GANADOR!!!!");
             }
             else if(saldoJ3 > saldoJ1 && saldoJ3 > saldoJ2 && saldoJ3 > saldoJ4 && saldoJ3 > saldoJ5 && saldoJ3 > saldoJ6)
                 JOptionPane.showConfirmDialog(null, VentanaInicial.jugador3.getNombre()+" es el GANADOR!!!!");
             else if(saldoJ4 > saldoJ1 && saldoJ4 > saldoJ2 && saldoJ4 > saldoJ3 && saldoJ4 > saldoJ5 && saldoJ4 > saldoJ6){
                 JOptionPane.showConfirmDialog(null, VentanaInicial.jugador4.getNombre()+" es el GANADOR!!!!");
             }
             else if(saldoJ5 > saldoJ1 && saldoJ5 > saldoJ2 && saldoJ5 > saldoJ3 && saldoJ5 > saldoJ4 && saldoJ5 > saldoJ6){
                 JOptionPane.showConfirmDialog(null, VentanaInicial.jugador5.getNombre()+" es el GANADOR!!!!"); 
             }
             else if(saldoJ6 > saldoJ1 && saldoJ6 > saldoJ2 && saldoJ6 > saldoJ3 && saldoJ6 > saldoJ4 && saldoJ6 > saldoJ5){
                 JOptionPane.showConfirmDialog(null, VentanaInicial.jugador6.getNombre()+" es el GANADOR!!!!");
             }        
        }
     
   
   
           
    }
     
     public static int Dados(){
         int n = 2 + (int)Math.random()*10;
         return n;
     }
}
