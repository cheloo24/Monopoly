
package Modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class ConexionTablero {
    Connection conexion;
    Statement consulta;
    private String ruta;
    
    public ConexionTablero(){
        ruta = "Propiedades.bd";
    }
    public void conectar(){
	try {
            Class.forName("org.sqlite.JDBC");
	}
	catch (ClassNotFoundException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
	}	 
            try {
                conexion = DriverManager.getConnection("jdbc:sqlite:"+ruta);
                consulta = conexion.createStatement();
            } 
            catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e.getMessage());
            }
    }
    
    public String consultaNombre(String numero){
        conectar();
        String NombreCasilla = "";
        try {
            String consultas = "select * from Casillas;";
            ResultSet rs = consulta.executeQuery(consultas);
            while(rs.next()){
                if (rs.getString("Numero").compareTo(numero) == 0){
                    NombreCasilla = rs.getString("Nombre");
                }
            }
        }
        catch (SQLException ex){
            JOptionPane.showMessageDialog(null, "Problemas al consultar la base de datos.");
        }
    return NombreCasilla;
    }
    
    public String consultaPrecio(String numero){
        conectar();
        String PrecioCasilla = "";
        try {
            String consultas = "select * from Casillas;";
            ResultSet rs = consulta.executeQuery(consultas);
            while(rs.next()){
                if (rs.getString("Numero").compareTo(numero) == 0){
                    PrecioCasilla = rs.getString("Precio");
                }
            }
        }
        catch (SQLException ex){
            JOptionPane.showMessageDialog(null, "Problemas al consultar la base de datos.");
        }
    return PrecioCasilla;
    }
    
    public String consultaRenta(String numero){
        conectar();
        String RentaCasilla = "";
        try {
            String consultas = "select * from Casillas;";
            ResultSet rs = consulta.executeQuery(consultas);
            while(rs.next()){
                if (rs.getString("Numero").compareTo(numero) == 0){
                    RentaCasilla = rs.getString("Renta");
                }
            }
        }
        catch (SQLException ex){
            JOptionPane.showMessageDialog(null, "Problemas al consultar la base de datos.");
        }
    return RentaCasilla;
    }
    
    public String consultaPropietario(String numero){
        conectar();
        String PropietarioCasilla = "";
        try {
            String consultas = "select * from Casillas;";
            ResultSet rs = consulta.executeQuery(consultas);
            while(rs.next()){
                if (rs.getString("Numero").compareTo(numero) == 0){
                    PropietarioCasilla = rs.getString("Propietario");
                    if (PropietarioCasilla.compareTo("false") == 0){
                        PropietarioCasilla = "Banco";
                    }
                }
            }
        }
        catch (SQLException ex){
            JOptionPane.showMessageDialog(null, "Problemas al consultar la base de datos.");
        }
    return PropietarioCasilla;
    }
    
    public void actualizaPropiedad (String nombre){
        try {
            conectar();
            String consultas = "select * froma Jugadores";
            ResultSet rs = consulta.executeQuery(consultas);
            while (rs.next()){
                if (rs.getString("Nombre").compareTo(nombre) == 0){
                    String sql = "UPDATE Propiedades set Propietario = true where Nombre='"+nombre+"';";
                    consulta.executeUpdate(sql);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConexionTablero.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
