/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Chelo
 */
public class CartasComunidad {
    private String[] comunidad = new String[10];
    
    public void LeeCartas() throws IOException{
        
        File archivo = new File("CartasComunidad.txt");        
        try {
            FileReader fr = new FileReader(archivo);
            BufferedReader br = new BufferedReader(fr);
            while(br.ready()){
                int i = 0;
                String linea = br.readLine();
                comunidad[i] = linea;
                i++;                
            }
            br.close();
            fr.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CartasComunidad.class.getName()).log(Level.SEVERE, null, ex);
        }       
        
    }
    
}
